package com.company;

public class ReaderEnd extends Thread {

    private Data data;

    public ReaderEnd(Data data) {
        super();
        this.data = data;
    }

    @Override
    public void run() {
        while(true) {
            this.data.readFromEnd();
        }
    }
}
