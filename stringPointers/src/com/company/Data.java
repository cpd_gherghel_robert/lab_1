package com.company;

public class Data {
    String sharedString = new String("ABCDEFGHIJK");
    int ps = 0;
    int pe = sharedString.length() - 1;
    boolean first = true;

    public synchronized void readFromStart() {
        if(ps < sharedString.length()) {
            while (!first) {
                try {
                    System.out.println("Reader S is waiting");
                    wait();
                    System.out.println("Reader S was notified");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            try {
                System.out.println("Reader S: " + sharedString.charAt(ps));
                ps++;
                first = false;
                notifyAll();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public synchronized void readFromEnd() {
        if(pe >= 0) {
            while (first) {
                try {
                    System.out.println("Reader E is waiting");
                    wait();
                    System.out.println("Reader E was notified");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            try {
                System.out.println("Reader E: " + sharedString.charAt(pe));
                pe--;
                first = true;
                notifyAll();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
