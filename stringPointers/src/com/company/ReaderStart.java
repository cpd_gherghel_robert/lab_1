package com.company;

public class ReaderStart extends Thread {

    private Data data;

    public ReaderStart(Data data) {
        super();
        this.data = data;
    }

    @Override
    public void run() {
        while(true) {
            this.data.readFromStart();
        }
    }
}
