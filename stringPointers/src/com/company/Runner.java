package com.company;

public class Runner {

    public static void execute() {
        Data data = new Data();
        ReaderStart readerStart = new ReaderStart(data);
        ReaderEnd readerEnd = new ReaderEnd(data);

        readerStart.start();
        readerEnd.start();
    }
}
