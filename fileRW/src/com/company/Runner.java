package com.company;

import java.io.File;
import java.io.FileReader;
import java.util.Scanner;

public class Runner {

    public static void execute() {
        File file = new File("RWfile.txt");
        Data data = new Data(file);

        for (int i = 5; i < 10; i++) {
            try {
                Scanner fileReader = new Scanner(file);
                Reader reader = new Reader(data, i, fileReader);
                reader.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        for (int i = 0; i < 3; i++) {
            Writer writer = new Writer(data, i);
            writer.start();
        }
    }
}
