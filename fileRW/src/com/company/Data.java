package com.company;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Data {
    private File file;
    private FileWriter fileWriter;
    private boolean writing = false;

    public Data(File file) {
        this.file = file;
    }

    public synchronized void write(String toWrite) {
        while(!writing) {
            try {
                System.out.println("Writer is waiting");
                wait();
                System.out.println("Writer was notified");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
            fileWriter = new FileWriter(file.getName(), true);
            fileWriter.write(toWrite);
            fileWriter.close();
            System.out.println("Wrote: " + toWrite);
            System.out.println("Writer notifies all threads");
            writing = false;
            notifyAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized String read(int number, Scanner fileReader) {
        String text = null;
        while (writing) {
            try {
                System.out.println("Reader" + number + " is waiting");
                wait();
                System.out.println("Reader" + number + " was notified");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
        //    fileReader = new Scanner(file);
            text = fileReader.nextLine();
        //    fileReader.close();
            System.out.println("Reader" + number + " has read: " + text);
            System.out.println("Reader" + number + " notifies all threads");
            writing = true;
            notifyAll();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return text;
    }
}
