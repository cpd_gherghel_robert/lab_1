package com.company;

import java.util.Scanner;

public class Reader extends Thread {
    private Data data;
    private int number;
    private Scanner fileReader;

    public Reader(Data data, int number, Scanner fileReader) {
        super();
        this.data = data;
        this.number = number;
        this.fileReader = fileReader;
    }

    @Override
    public void run() {
        while (true) {
            String read = data.read(number, fileReader);
        //    System.out.println("R" + number + " has read: " + read + "\n");
        }
    }
}
