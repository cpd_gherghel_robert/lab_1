package com.company;

import java.util.Random;

public class Writer extends Thread {
    private Data data;
    private Random rand;
    private int number;

    public Writer(Data data, int number) {
        super();
        this.data = data;
        rand = new Random();
        this.number = number;
    }

    @Override
    public void run() {
        for(int i = 0; i < 10; i++) {
            StringBuilder toWrite = new StringBuilder();
            toWrite.append("W" + number + " - line" + i /*rand.nextInt(100)*/ + "\n");
        //    System.out.println("W" + number + " is writing: " + toWrite.toString());
            data.write(toWrite.toString());
        }
    }
}
