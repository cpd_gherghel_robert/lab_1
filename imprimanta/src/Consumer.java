public class Consumer extends Thread {
    private Data data;

    public Consumer(Data data) {
        super();
        this.data = data;
    }

    public void run() {
        while(true) {
            print();
        }
    }

    public void print() {
        try {
            while(!data.hasPackets()) {
                System.out.println("Nothing to print");
                sleep(500);
            }
            while(data.hasPackets()) {
                String packet = data.consume();
                System.out.println("Printer prints: " + packet);
                sleep(500);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
