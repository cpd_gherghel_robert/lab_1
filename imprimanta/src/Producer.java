import java.util.Random;

public class Producer extends Thread{
    private long delay;
    private Data data;
    private int number;
    private int randDocs = 0;

    public Producer(long delay, Data data, int number) {
        super();
        this.delay = delay;
        this.data = data;
        this.number = number;
        Random rand = new Random();
        randDocs = rand.nextInt(3) + 1;
    }

    public void run() {
        try {
            for(int i = 0; i <= randDocs; i++) {
                sleep(delay);
                String packet = "doc: " + i + " - c" + number;
                System.out.println("Clerk " + number + " writes doc " + i);
                System.out.println(data.toString());
                data.produce(packet);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
