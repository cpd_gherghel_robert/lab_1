import java.util.Random;

public final class Runner {
    public static void execute() {
        Data queue = new Data();
        Random rand = new Random();
        System.out.println("Initialize and start threads synchronized by queue");

        Consumer c = new Consumer(queue);
        for(int i = 0; i < 8; i++) {
            Producer p = new Producer(rand.nextInt(3000), queue, i);
            p.start();
        }
        c.start();
    }
}
